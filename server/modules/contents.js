const random = require('./../../helpers/common/random');
const persons = require('./../../helpers/collections/persons');

const mockViewer = require('./../mocks/content');

let uniqId = 0;
let content = {};

const generateContent = (io) => {
	uniqId = uniqId + 1;
	const content = mockViewer.createMock(uniqId, 'start');
	console.log('generate content');
	io.emit('content', content);

	return content;
};

const triggerEventsForContent = (content, io) => {

	let removeContentTimeout = setTimeout(() => {
		console.log('remove content');
		content = Object.assign({}, content, {name_of_event: 'end'});
		io.emit('content', content);

		content = Object.assign({}, generateContent(io));
		removeContentTimeout = triggerEventsForContent(content, io);
	}, random.getIntBetween(10000, 20000));
	return removeContentTimeout;
};

const viewersModule = (io) => {

	return {
		init: () => {
			const content = generateContent(io);
			triggerEventsForContent(content, io);
		},
	};
};

module.exports = viewersModule;
