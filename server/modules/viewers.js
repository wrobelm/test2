const random = require('./../../helpers/common/random');
const persons = require('./../../helpers/collections/persons');

const mockViewer = require('./../mocks/viewer');

let uniqId = 0;
const addPerson = (io) => {
	uniqId = uniqId + 1;
	const personsMap = persons.setPerson(uniqId, mockViewer.createMock(uniqId));

	io.emit('all persons', personsMap.toArray());

	return persons.getPerson(uniqId);
};

const triggerEventsForPerson = (person, io) => {

	let updateTimeout;
	const updateUser = ()=> {
		const personCopy = persons.getPerson(person.person_id);

		if (personCopy) {
			const personsMap = updatePersonsData(personCopy);
			io.emit('all persons', personsMap.toArray());

			updateTimeout = setTimeout(updateUser, random.getIntBetween(50, 200));
		}
	};

	updateTimeout = setTimeout(updateUser, random.getIntBetween(50, 200));


	const deleteTimeout = setTimeout(() => {
		console.log('remove person');
		const personsMap = removePerson(person);
		io.emit('all persons', personsMap.toArray());
	}, random.getIntBetween(3000, 60000));
};

const updatePersonsData = (person) => {
	person = Object.assign({}, person, {
		local_timestamp: (new Date()).getTime(),
	});
	return persons.setPerson(person.person_id, person);
};

const removePerson = (person) => {
	return persons.deletePerson(person.person_id);
};

const viewersModule = (io) => {

	return {
		addPerson: () => {
			const person = addPerson(io);
			triggerEventsForPerson(person, io);
		},
	};
};

module.exports = viewersModule;
