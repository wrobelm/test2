const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const socketio = require('socket.io');
const Rx = require('rx');

const persons = require('./../helpers/collections/persons');

const ViewersModule = require('./modules/viewers');
const ContentsModule = require('./modules/contents');

const app = express();

const hostname = process.env.HOSTNAME || 'localhost';
const port = parseInt(process.env.PORT, 10) || 3000;

app.use(express.static(__dirname + '/../public'));
app.use(bodyParser.json());

app.all('/', (req, res) => {
	res.send(fs.readFileSync(__dirname + '/../public/index.html', 'utf8'))
});

const server = app.listen(port, hostname, () => {
	const _host = server.address().address;
	const _port = server.address().port;

	console.log('App listening at http://%s:%s', _host, _port);
});

const io = socketio(server);
const viewersModule = ViewersModule(io);
const contentsModule = ContentsModule(io);

const sourceConnect = Rx.Observable.create((observer) => {

	io.on('connection', (socket) => {
		socket.on('client connect', (data) => {
			observer.onNext({socket, data, event: 'client connect'});
		});
	});

	return () => {
		io.close();
	};
});

const sourceDisconnect = Rx.Observable.create((observer) => {

	io.on('connection', (socket) => {
		socket.on('disconnect', (data) => {
			observer.onNext({socketId: socket.id, event: 'client disconnect'});
		});
	});

	return () => {
		io.close();
	};
});

const observerConnect = sourceConnect
	.subscribe((obj) => {
		contentsModule.init();
		console.log('connected');
	});

const observerDisconnect = sourceDisconnect
	.subscribe((obj) => {
		console.log('disconnected');
	});

app.post('/addPerson', (req, res) => {
	console.log('add person');
	viewersModule.addPerson();

	res.writeHead(200, {'Content-Type': 'application/json'});
	res.write(JSON.stringify({status: 'OK'}));
	res.end();
});