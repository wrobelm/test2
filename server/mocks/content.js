const random = require('./../../helpers/common/random');

const schema = {
	"title": "content_event",
	"type": "object",
	"properties": {
		"local_timestamp": {
			"type": "number",
			"description": "Timestamp in unix epoch format"
		},
		"name_of_event": {
			"type": "string",
			"description": "e.g., start, end, abort"
		},
		"content_id": {
			"type": "string",
			"description": "unique identifier of a content"
		},
		"content_name": {
			"type": "string",
			"description": "the name of the content/file that is playing"
		},
	},
	"required": [
		"local_timestamp",
		"name_of_event",
		"content_id",
		"content_name"
	]
};

const contentType = [
	'video',
	'image',
];

const createMock = (id, event) => {
	return {
		local_timestamp: (new Date()).getTime(),
		content_id: id,
		content_name: `${contentType[random.getIntBetween(0,1)]}${id}`,
		name_of_event: event,
	};
};


module.exports = {
	createMock,
};
