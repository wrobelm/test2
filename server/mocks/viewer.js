const random = require('./../../helpers/common/random');

const schema = {
	"title": "person",
	"type": "object",
	"properties": {
		"local_timestamp": {
			"type": "number",
			"description": "Timestamp in unix epoch format"
		},
		"person_id": {
			"type": "string",
			"description": "Unique per person. Multiple messages can be assigned to one person_id"
		},
		"person_put_id": {
			"type": "string",
			"description": "Globally unique id among all person messages"
		},
		"rolling_expected_values": {
			"type": "object",
			"properties": {
				"age": {
					"type": "number"
				},
				"gender": {
					"type": "string",
					"enum": [
						"male",
						"female"
					]
				}
			},
			"required": [
				"age",
				"gender"
			]
		},
		"required": [
			"local_timestamp",
			"person_id",
			"person_put_id",
			"rolling_expected_values",
		]
	}
};

const gender = [
	'male',
	'female'
];

const createMock = (id) => {
	return {
		local_timestamp: (new Date()).getTime(),
		person_id: id,
		person_put_id: id,
		age: random.getIntBetween(15,100),
		gender: gender[random.getIntBetween(0,1)],
	};
};


module.exports = {
	createMock,
};
