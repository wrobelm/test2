const Immutable = require('immutable');
let usersMap = Immutable.Map({});

module.exports = {
	setUser: (id, data) => {
		usersMap = usersMap.set(id, data);
		return usersMap;
	},
	deleteUser: (id) => {
		usersMap = usersMap.delete(id);
		return usersMap;
	},
};
