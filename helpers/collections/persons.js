const Immutable = require('immutable');
let personsMap = Immutable.Map({});

module.exports = {
	setPerson: (id, data) => {
		personsMap = personsMap.set(id, data);
		return personsMap;
	},

	deletePerson: (id) => {
		personsMap = personsMap.delete(id);
		return personsMap;
	},

	getPerson: (id) => {
		return personsMap.get(id);
	},

	getPersons: () => {
		return personsMap;
	},
};
