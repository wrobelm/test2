const Immutable = require('immutable');
var contentsMap = [];

module.exports = {
	setContent: (id, data) => {
		contentsMap.push(data);
		return contentsMap;
	},

	getContent: (id) => {
		return contentsMap.find(content => content.content_id === id);
	},

	getContents: () => {
		return contentsMap;
	},

	getPrevious: () => {
		return contentsMap[contentsMap.length - 2];
	},

	getLastContent: () => {
		return contentsMap[contentsMap.length - 1];
	},
};
