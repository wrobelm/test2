import Rx from 'rx';
import {getElement} from './dom';
import {socket, observables} from './streams';
import {template as appTemplate} from './templates/app';
import {template as personsTemplate} from './templates/persons';
import {template as pastStatsTemplate} from './templates/pastStats';
import {template as currentContentTemplate} from './templates/currentContent';

import * as contentsModule from './modules/contents';

const button = getElement('button');
const appRegion = getElement('.app--region');

const clickStream = Rx.Observable.fromEvent(button, 'click');

appRegion.innerHTML = appTemplate();
const personsRegion = getElement('.persons--region');
const pastStatsRegion = getElement('.past-stats--region');
const currentStatsRegion = getElement('.current-stats--region');

clickStream.subscribe(() => {
	fetch('/addPerson', {method: 'POST'});
});

socket.on('connect', () => {
	console.log('connected');
	socket.emit('client connect');
});

let persons;
observables.socketAllPersonsStream.subscribe(data => {
	persons = data;
	const content = contentsModule.getLastContent();
	if (content) {
		contentsModule.setPersonsToContent(content.content_id, data);
	}
	personsRegion.innerHTML = personsTemplate(data);
});

observables.socketContentStream.subscribe(data => {
	if (data && data.name_of_event === 'start') {
		contentsModule.addContent(data.content_id, data);

		currentStatsRegion.innerHTML = currentContentTemplate(data);
		if (contentsModule.getSize() > 1) {
			const stats = contentsModule.getStatsOfPrevious();
			pastStatsRegion.innerHTML = pastStatsTemplate(stats);
		}

	}
});
