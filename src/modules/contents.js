import Immutable from 'immutable';
import contents from './../../helpers/collections/contents';

const countAverage = (arr) => {
	const sum = arr.reduce(function (a, b) {
		return a + b;
	});
	return sum / arr.length;
};

export const addContent = (id, data) => {
	contents.setContent(id, data);
};

export const setPersonsToContent = (id, persons) => {
	const content = contents.getContent(id);

	if (!Array.isArray(content.persons)) {
		content.persons = [];
	}

	persons.forEach((person) => {
		if (!content.persons.find(contentperson => contentperson.person_id === person.person_id)) {
			content.persons.push(person);
		}
	});
};

export const getStatsOfPrevious = () => {
	const previousContent = contents.getPrevious();
	if (!previousContent.persons) {
		return {
			content_id: previousContent.content_id,
			content_name: previousContent.content_name,
		};
	}

	const persons = previousContent.persons;

	let males = [];
	let females = [];

	persons.forEach(person => {
		if (person.gender === 'male') {
			males.push(person.age);
		} else {
			females.push(person.age);
		}
	});

	return {
		content_id: previousContent.content_id,
		content_name: previousContent.content_name,
		total: persons.length,
		malesTotal: males.length,
		femalesTotal: females.length,
		malesAverageAge: males.length ? countAverage(males) : 0,
		femalesAverageAge: females.length ? countAverage(females) : 0,
	};
};

export const getLastContent = () => {
	return contents.getLastContent();
};

export const getSize = () => {
	return getAll().length;
};

export const getAll = () => {
	return contents.getContents();
}