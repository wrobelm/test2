import io from 'socket.io-client';
import Rx from 'rx';

export const socket = io();

export const observables = {
	socketAllPersonsStream: Rx.Observable.create(observer => {
		socket.on('all persons', data => {
			observer.onNext(data);
		});
	}),

	socketContentStream: Rx.Observable.create(observer => {
		socket.on('content', data => {
			observer.onNext(data);
		});
	}),

};
