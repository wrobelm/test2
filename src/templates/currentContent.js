export const template = (stats) => {
	return `
		<h4>Current content</h4>
		<ul class="past-stats-list">
			<li>id: ${stats.content_id}</li>
			<li>name: ${stats.content_name}</li>
		</ul>
	`
}