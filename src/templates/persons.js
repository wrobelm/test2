export const personTemplate = (person) => {

	const personId = person.person_id;
	const age = person.age;
	const gender = person.gender;

	return `
	<li class="person">id: ${personId} age: ${age} gender: ${gender}</li>
	`;
};

export const template = (persons) => {
	return `
		<ul class="persons-list">
			${persons.map(personTemplate).join(' ')}
		</ul>
	`
}