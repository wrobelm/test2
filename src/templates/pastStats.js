export const template = (stats) => {
	return `
		<h4>Past Stats</h4>
		<ul class="past-stats-list">
			<li>id: ${stats.content_id}</li>
			<li>name: ${stats.content_name}</li>
			<li>total persons ${stats.total}</li>
			<li>total males ${stats.malesTotal}</li>
			<li>males average age ${stats.malesAverageAge}</li>
			<li>total females ${stats.femalesTotal}</li>
			<li>females average age ${stats.femalesAverageAge}</li>
		</ul>
	`
}