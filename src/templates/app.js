export const template = () => {
	return `
		<div class="persons--region"></div>
		<div class="current-stats--region"></div>
		<div class="past-stats--region"></div>
	`;
};